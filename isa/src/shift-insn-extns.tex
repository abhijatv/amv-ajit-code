\subsubsection{Shift instructions:}
\label{sec:shift:insn:impl}
The shift  family of instructions  of AJIT  may each be  considered to
have  two versions:  a direct  count version  and a  register indirect
count version.  In the direct count  version the shift count is a part
of the  instruction bits.   In the indirect  count version,  the shift
count is  found on the  register specified by  the bit pattern  in the
instruction  bits.   The direct  count  version  is specified  by  the
14$^{th}$  bit, i.e.  insn[13]  (bit  number 13  in  the  0 based  bit
numbering scheme), being set to 1.  If insn[13] is 0 then the register
indirect version is specified.

Similar to the addition and subtraction instructions, the shift family
of instructions of  SPARC V8 also do  not use bits from 5  to 12 (both
inclusive).  The AJIT processor uses bits  5 and 6.  In particular bit
6 is always 1.   Bit 5 may be used in the direct  version giving a set
of 6 bits  available for specifying the shift count.   The shift count
can have  a maximum  value of  64.  Bit  5 is  unused in  the register
indirect version, and is always 0 in that case.

These instructions  are therefore  worked out  below in  two different
sets: the direct and the register indirect ones.
\begin{enumerate}
\item The direct versions  are given by insn[13] = 1.  The 6 bit shift
  count  is directly  specified  in the  instruction bits.   Therefore
  insn[5:0] specify the  shift count.  insn[6] =  1, distinguishes the
  AJIT version from the SPARC V8 version.
  \begin{enumerate}
  \item \textbf{SLLD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} & \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Lowest 5 bits of shift count \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Use bit 5
                                              to specify the msb of
                                              shift count.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``1''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100101 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SLLD}: same as SLL, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{slld SrcReg1, 6BitShiftCnt,
        DestReg}''. \\
      (\textbf{Note:} In an assembly language program, when the second
      argument is a number, we have direct mode.  A register number is
      prefixed with  ``r'', and hence the  syntax itself distinguished
      between   direct  and   register   indirect   version  of   this
      instruction.)
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $<<$
      shift count.
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0010  1        1           1        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0010  1SSS   SS1U  UUUU   U1II  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x25     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x1      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.

  \item \textbf{SRLD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} & \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Lowest 5 bits of shift count \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Use bit 5
                                              to specify the msb of
                                              shift count.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``1''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100110 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SRLD}: same as SRL, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{sral SrcReg1, 6BitShiftCnt,
        DestReg}''. \\
      (\textbf{Note:} In an assembly language program, when the second
      argument is a number, we have direct mode.  A register number is
      prefixed with  ``r'', and hence the  syntax itself distinguished
      between   direct  and   register   indirect   version  of   this
      instruction.)
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $>>$
      shift count.
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0011  0        1           1        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0011  0SSS   SS1U  UUUU   U1II  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x26     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x1      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.
    
  \item \textbf{SRAD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} & \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Lowest 5 bits of shift count \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Use bit 5
                                              to specify the msb of
                                              shift count.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``1''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100111 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SRAD}: same as SRA, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{srad SrcReg1, 6BitShiftCnt,
        DestReg}''. \\
      (\textbf{Note:} In an assembly language program, when the second
      argument is a number, we have direct mode.  A register number is
      prefixed with  ``r'', and hence the  syntax itself distinguished
      between   direct  and   register   indirect   version  of   this
      instruction.)
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $>>$
      shift count (with sign extension).
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0011  1        1           1        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0011  1SSS   SS1U  UUUU   U1II  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x27     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x1      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.

  \end{enumerate}
\item The register  indirect versions are given by insn[13]  = 0.  The
  shift count is indirectly specified in the 32 bit register specified
  in instruction bits.  Therefore  insn[4:0] specify the register that
  has the  shift count.  insn[6]  = 1, distinguishes the  AJIT version
  from the SPARC V8 version.  In this case, insn[5] = 0, necessarily.
  \begin{enumerate}
  \item \textbf{SLLD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} &
                                                                            \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Register number \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Set bit 5 to 0.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``0''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100101 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SLLD}: same as SLL, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{slld SrcReg1, SrcReg2,
        DestReg}''.
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $<<$
      shift count register rs2.
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0010  1        0           10        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0010  1SSS   SS0U  UUUU   U10I  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x25     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x0      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.

  \item \textbf{SRLD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} &
                                                                            \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Register number \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Set bit 5 to 0.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``0''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100110 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SRLD}: same as SRL, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{slld SrcReg1, SrcReg2,
        DestReg}''.
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $>>$
      shift count register rs2.
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0011  0        0           10        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0011  0SSS   SS0U  UUUU   U10I  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x26     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x0      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.

  \item \textbf{SRAD}:\\
    \begin{center}
      \begin{tabular}[p]{|c|c|l|l|p{.35\textwidth}|}
        \hline
        \textbf{Start} & \textbf{End} & \textbf{Range} & \textbf{Meaning} &
                                                                            \textbf{New Meaning}\\
        \hline
        0 & 4 & 32 & Source register 2, rs2 & Register number \\
        \hline
        5 & 12 & -- & \textbf{unused} &
                                        \begin{minipage}[h]{1.0\linewidth}
                                          \begin{itemize}
                                          \item \textbf{Set bit 5 to 0.}
                                          \item \textbf{Use bit 6 to
                                              distinguish AJIT from
                                              SPARC V8.}
                                          \end{itemize}
                                        \end{minipage}
        \\
        \hline
        13 & 13 & 0,1 & The \textbf{i} bit & \textbf{Set i to ``0''} \\
        14 & 18 & 32 & Source register 1, rs1 & No change \\
        19 & 24 & 100101 & ``\textbf{op3}'' & No change \\
        25 & 29 & 32 & Destination register, rd & No change \\
        30 & 31 & 4 & Always ``10'' & No change \\
        \hline
      \end{tabular}
    \end{center}
    \begin{itemize}
    \item []\textbf{SRAD}: same as SRA, but with Instr[13]=0 (i=0),
      and Instr[5]=1.
    \item []\textbf{Syntax}: ``\texttt{slld SrcReg1, SrcReg2,
        DestReg}''.
    \item []\textbf{Semantics}: rd(pair) $\leftarrow$ rs1(pair) $>>$
      shift count register rs2 (with sign extension).
    \end{itemize}
    Bits layout:
\begin{verbatim}
    Offsets      : 31       24 23       16  15        8   7        0
    Bit layout   :  XXXX  XXXX  XXXX  XXXX   XXXX  XXXX   XXXX  XXXX
    Insn Bits    :  10       1  0011  1        0           10        
    Destination  :    DD  DDD                                       
    Source 1     :                     SSS   SS
    Source 2     :                                           S  SSSS
    Unused (0)   :                              U  UUUU   UU        
    Final layout :  10DD  DDD1  0011  1SSS   SS0U  UUUU   U10I  IIII
\end{verbatim}

    This will need another macro that sets bits 5 and 6. Let's call it
    \texttt{OP\_AJIT\_BIT\_2}.   Hence the  SPARC bit  layout of  this
    instruction is:

    \begin{tabular}[h]{lclcl}
      Macro to set  &=& \texttt{F5(x, y, z)} &in& \texttt{sparc.h}     \\
      Macro to reset  &=& \texttt{INVF5(x, y, z)} &in& \texttt{sparc.h}     \\
      x &=& 0x2      &in& \texttt{OP(x)  /* ((x) \& 0x3)  $<<$ 30 */} \\
      y &=& 0x27     &in& \texttt{OP3(y) /* ((y) \& 0x3f) $<<$ 19 */} \\
      z &=& 0x0      &in& \texttt{F3I(z) /* ((z) \& 0x1)  $<<$ 13 */} \\
      a &=& 0x2      &in& \texttt{OP\_AJIT\_BIT\_2(a) /* ((a) \& 0x3  $<<$ 6 */}
    \end{tabular}

    The AJIT bits (insn[6:5]) is  set or reset internally by \texttt{F5}
    (just  like  in  \texttt{F4}),  and   hence  there  are  only  three
    arguments.
  \end{enumerate}
\end{enumerate}
