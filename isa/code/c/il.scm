(define register-types
  '('('description-type BIT_FLAG)
    '(SPARC_GENERAL_PURPOSE
      SPARC_DATA_REGISTER
      SPARC_CONTROL_REGISTER
      SPARC_STATUS_REGISTER
      ALL_SPARC_REGISTER_TYPES
      )
    ))
(define register-file
  '(()
    ))
(define bit-region-meanings-list
  '(
    SPARC_INSN_FORMAT
    SPARC_DESTINATION_REGISTER
    SPARC_SOURCE_REGISTER_ONE
    SPARC_SOURCE_REGISTER_TWO
    SPARC_OP_FORMAT_ONE
    SPARC_OP_FORMAT_TWO
    SPARC_OP_FORMAT_THREE
    SPARC_ANNUL_BIT
    SPARC_CONDITION_CODE_MASK
    SPARC_IMMEDIATE_22
    SPARC_DISPLACEMENT_22
    SPARC_DISPLACEMENT_30
    SPARC_BIT_I		;; ALU second operand selector.
    SPARC_ASI		;; address space identifier.
    SPARC_SIMM_13		;; Signed Immediate. Depends on SPARC_BIT_I.
    SPARC_OPF		;; Floating point op, or CoProcessor op.
    ALL_SPARC_REGION_MEANINGS
    ))

(define alignment-types
  '(
    ALIGN_BYTE		;;  1 Byte
    ALIGN_HALF_WORD		;;  2 Bytes, 1 Half word
    ALIGN_WORD		;;  4 Bytes, 2 Half words, 1 Word
    ALIGN_DOUBLE_WORD		;;  8 Bytes, 4 Half words, 2 Words, 1 Double word
    ALIGN_QUAD_WORD		;; 16 Bytes, 8 Half words, 4 Words, 2 Double words, 1 Quada word
    ))

(define bit-order
  '(
    LITTLE_ENDIAN
    BIG_ENDIAN		;; SPARC is BIG Endian
    ))

(define insn-subtype
  '(
    SPARC_LOAD_STORE		;; SPARC V8: Category 1.
    SPARC_INTEGER_ARITHMETIC		;; SPARC V8: Category 2.
    SPARC_CONTROL_TRANSFER		;; SPARC V8: Category 3.
    SPARC_RW_CONTROL_REGISTER		;; SPARC V8: Category 4.
    SPARC_FLOAT_PT_OP		;; SPARC V8: Category 5.
    SPARC_COPROC_OP		;; SPARC V8: Category 6.
    AJIT_ADD_SUB		;; AJIT Category 1.
    AJIT_SHIFT		;; AJIT Category 2.
    AJIT_MUL_DIV		;; AJIT Category 3.
    AJIT_64BIT_LOGICAL		;; AJIT Category 4.
    AJIT_SIMD_I		;; AJIT Category 5.
    AJIT_SIMD_II		;; AJIT Category 6.
    AJIT_SIMD_FLOAT		;; AJIT Category 7.
    ALL_AJIT_INSN_GROUPS		;; This must  be last. It  denotes the number of groups.
    ))

(define bit-field-type
  '(
    USED		;; Used and not ignored; these mean something
    REGISTER		;; Specify some registers
    OPCODE		;; Used to build the op code
    IGNORED		;; Used but ignored, e.g. op code bits
    LOCATION		;; Used to specify primary memory location
    UNUSED
    ALL_AJIT_BIT_FIELD_TYPES
    ))

(define value-representation
  '(BINARY
    OCTAL
    DECIMAL
    HEXADECIMAL
    ))

(define insn-structure
  '(('mnemonic . "ADD")
    ('insn-subtype . SPARC_INTEGER_ARITHMETIC)
    ('alignment-types . ALIGN_WORD)
    ('length-in-bits  . 32)
    ('bit-order . BIG)
    ('opcode-layout . '('(('region-name . "op")
			  ('region-start . 30)
			  ('region-end . 31)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_OP_FORMAT_ONE)
			  ('region-setup . '(('value . 10)
					     ('value-representation . BINARY)
					     ('value-type . OPCODE)
					     )))
			'(('region-name . "rd")
			  ('region-start . 25)
			  ('region-end . 29)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_DESTINATION_REGISTER)
			  ('region-setup . '(('value . XXXXX) ; CAREFUL HERE! How is this set?
					     ('value-representation . BINARY)
					     ('value-type . REGISTER)
					     )))
			'(('region-name . "op3")
			  ('region-start . 19)
			  ('region-end . 24)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_OP_FORMAT_THREE)
			  ('region-setup . '(('value . 10)
					     ('value-representation . BINARY)
					     ('value-type . OPCODE)
					     )))
			'(('region-name . "rs1")
			  ('region-start . 14)
			  ('region-end . 18)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_SOURCE_REGISTER_ONE)
			  ('region-setup . '(('value . 10)
					     ('value-representation . BINARY)
					     ('value-type . OPCODE)
					     )))
			'(('region-name . "i")
			  ('region-start . 13)
			  ('region-end . 13)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_BIT_I)
			  ('region-setup . '(('value . 10)
					     ('value-representation . BINARY)
					     ('value-type . OPCODE)
					     )))
			'(('region-name . "unused")
			  ('region-start . 5)
			  ('region-end . 12)
			  ('region-length . (region-end - region-start + 1))
			  ('region-meaning . SPARC_BIT_I)
			  ('region-setup . '(('value . 10)
					     ('value-representation . BINARY)
					     ('value-type . OPCODE)
					     )))
			))
    ))


  ;; strcpy (insns[0].mnemonic, "ADDD");
  ;; insns[0].subtype = AJIT_ADD_SUB;
  ;; insns[0].num_bits = 32;
  ;; insns[0].align = ALIGN_WORD;
  ;; insns[0].order = BIG;
  ;; strcpy (insns[0].layout[0].region.region_name, "op");
  ;; insns[0].layout[0].region.start = 30;
  ;; insns[0].layout[0].region.end = 31;
  ;; insns[0].layout[0].meaning = SPARC_OP_FORMAT_ONE;


