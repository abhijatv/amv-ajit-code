\contentsline {chapter}{\numberline {1}The ISA Specification from IITB}{7}{chapter.1}
\contentsline {section}{\numberline {1.1}Overview}{7}{section.1.1}
\contentsline {section}{\numberline {1.2}ISA Extensions}{8}{section.1.2}
\contentsline {subsection}{\numberline {1.2.1}Integer-unit extensions: Arithmetic-logic instructions}{8}{subsection.1.2.1}
\contentsline {subsection}{\numberline {1.2.2}Integer-unit extensions: SIMD instructions}{8}{subsection.1.2.2}
\contentsline {subsection}{\numberline {1.2.3}Integer-unit extensions: SIMD instructions}{8}{subsection.1.2.3}
\contentsline {subsection}{\numberline {1.2.4}Vector floating point instructions}{8}{subsection.1.2.4}
\contentsline {subsection}{\numberline {1.2.5}CSWAP instructions}{8}{subsection.1.2.5}
\contentsline {chapter}{\numberline {2}AJIT Support for the GNU Binutils Toolchain}{17}{chapter.2}
\contentsline {section}{\numberline {2.1}Towards a GNU Binutils Toolchain}{17}{section.2.1}
\contentsline {subsection}{\numberline {2.1.1}Opcode Bit Patterns}{17}{subsection.2.1.1}
\contentsline {chapter}{\numberline {3}Towards Assembler Extraction}{19}{chapter.3}
\contentsline {section}{\numberline {3.1}Succinct ISA Descriptions}{19}{section.3.1}
\contentsline {subsection}{\numberline {3.1.1}Instruction Set Design Study}{19}{subsection.3.1.1}
\contentsline {subsubsection}{Basic Concepts of Instruction Set Design}{19}{section*.3}
\contentsline {subsubsection}{Some Examples of Instruction Set Design Languages}{22}{section*.4}
\contentsline {subsection}{\numberline {3.1.2}Instruction Set Description and Generation}{22}{subsection.3.1.2}
\contentsline {subsubsection}{Basic Elements of the Structure of an Instruction Set Language}{22}{section*.5}
\contentsline {subsection}{\numberline {3.1.3}Instruction Set Generation}{23}{subsection.3.1.3}
\contentsline {subsubsection}{Basic Elements of the ``Language'' to Describe the Instruction}{23}{section*.6}
